//---------------------------------------------------------------------------
/**
* @file		cache.cpp
* @date 	4/20/2019
* @author	Alexandre Corcia Aguilera
* @par		Login: alexandre.corcia
* @par		Course: CS315
* @par		Assignment #5
* @brief 	The cache mechanics are here, here we have the Constructor
*			with all of the variable setting and allocating the memory
*			and some function on the work of the cache.
**/
//---------------------------------------------------------------------------
#include "cache.h"
#include <algorithm>

/**
* @brief The constructor of the class cache
* @param associ		the number of lines per set
* @param count		The number of sets
* @param b_size		The size of the block
* @param size		Total size of a line.
* @param policy		The Policy for how we will be treating the conflict misses
*/
CACHE::CACHE(const unsigned associ, const unsigned count, const unsigned b_size, const unsigned size, EvictionPolicy policy)
{
	//This function is pretty self explanatory, it starts all of the memory and sets it to the beginning
	this->associativity = associ;
	this->set_count = count;
	this->block_size = b_size;
	this->size = size;
	this->m_policy = policy;

	m_memory = new Memory<4>;

	for (unsigned i = 0; i < set_count; i++)
	{
		set * new_set = new set;

		for (unsigned j = 0; j < associativity; j++)
		{
			set::line * new_line = new set::line;

			new_line->block.resize(block_size);
			new_set->s_all_lines.push_back(new_line);
			new_set->s_al_frequencies.push_back(0);	
		}

		c_all_sets.push_back(new_set);
	}
}

/**
* @brief This function will check if the addressed passed is actually in the current cache or not.
* @param new_address		The address we need to check
* @return bool	if it is or not
*/
bool CACHE::is_address_cached(size_t new_address)
{
	auto set = (new_address >> 1) % set_count;

	unsigned tag = (new_address >> (4 - std::min(associativity, 3u))) % (associativity << 1);

	//Going through all of the line of the set found.
	for (unsigned i = 0; i < c_all_sets[set]->s_all_lines.size(); i++)
	{
		if (c_all_sets[set]->s_all_lines[i]->valid == true)
		{
			if (c_all_sets[set]->s_all_lines[i]->tag == tag)
				return true;
		}
	}
	return false;
}

/**
* @brief this will set the address passed in the cache and 
		 take it into account in the stats, this fucntion is called from the cpu
* @param stats		
* @param new_address
*/
void CACHE::set_adress(Statistics & stats, size_t new_address)
{
	auto set = (new_address >> 1) % set_count;

	unsigned tag = (new_address >> (4 - std::min(associativity, 3u))) % (associativity << 1);

	//Finding the where we should put the tag that we have made.
	for (unsigned i = 0; i < c_all_sets[set]->s_all_lines.size(); i++)
	{
		//Checking if all spots are used if not we set it to the first one not free
		if (c_all_sets[set]->s_all_lines[i]->valid == true)
		{
			if (c_all_sets[set]->s_all_lines[i]->tag == tag)
			{
				stats[Statistics::cache_hits]++;
				c_all_sets[set]->s_al_frequencies[i]++;
				past_lines.push(i);
				return;
			}
		}
		else
		{
			//Setting if the we find a spot in the cache that is unused, not valid.
			c_all_sets[set]->s_all_lines[i]->tag = tag;
			c_all_sets[set]->s_all_lines[i]->valid = true;
			
			//It is a cachen miss so we do put new memory and set it thanks to the class memory where the data is.
			for(unsigned j = 0; j < block_size; j++)
				c_all_sets[set]->s_all_lines[i]->block[j] = (*m_memory)[new_address];

			//Update the stats.
			stats[Statistics::cache_misses]++;
			stats[Statistics::cold_misses]++;
			past_lines.push(i);
			return;
		}
	}

	//If all of the spots are used we should update the stats 
	//and dependign on the policy update with the new values asked
	stats[Statistics::cache_misses]++;
	stats[Statistics::conflict_misses]++;

	switch (m_policy)
	{
	case EvictionPolicy::Line:
	{
		c_all_sets[set]->s_all_lines[0]->tag = tag;

		for (unsigned j = 0; j < block_size; j++)
			c_all_sets[set]->s_all_lines[0]->block[j] = (*m_memory)[new_address];

		break;
	}
	case EvictionPolicy::Random:
	{
		unsigned rand_line = rand() % c_all_sets[set]->s_all_lines.size();
		c_all_sets[set]->s_all_lines[rand_line]->tag = tag;

		for (unsigned j = 0; j < block_size; j++)
			c_all_sets[set]->s_all_lines[rand_line]->block[j] = (*m_memory)[new_address];

		break;
	}

	case EvictionPolicy::LRU:
	{
		unsigned int lru_line = past_lines.front();
		past_lines.pop();
		c_all_sets[set]->s_all_lines[lru_line]->tag = tag;
		past_lines.push(lru_line);

		for (unsigned j = 0; j < block_size; j++)
			c_all_sets[set]->s_all_lines[lru_line]->block[j] = (*m_memory)[new_address];


		break;
	}

	case EvictionPolicy::LFU:
	{
		unsigned LF = 0;

		for (unsigned i = 0; i < c_all_sets[set]->s_al_frequencies.size(); i++)
		{
			if (c_all_sets[set]->s_al_frequencies[i] < c_all_sets[set]->s_al_frequencies[LF])
				LF = i;
		}

		c_all_sets[set]->s_all_lines[LF]->tag = tag;
		c_all_sets[set]->s_al_frequencies[LF] = 0;

		for (unsigned j = 0; j < block_size; j++)
			c_all_sets[set]->s_all_lines[LF]->block[j] = (*m_memory)[new_address];

		break;
	}

	default:
		break;
	}
}

#pragma once
#include "cache.h"

class CPU
{
public:

	CPU() = default;
	CPU(unsigned a, unsigned sc, unsigned bs, unsigned s,CACHE::EvictionPolicy ep = CACHE::EvictionPolicy::Line);

	Statistics run(MemoryTrace m_trace);

	CACHE get_cache(unsigned int m_value);




private:

	CACHE * m_cache;
};

// this code will not compile with the provided material
// comment this whole file to make the project compile and pass the rest of the tests
// from that point, start adding code progressively

#include "cache.h"    // this header is not provided, but it is assummed that cache-related declarations go here
#include "cpu.h"      // this header is not provided, but it is assummed that CPU-related declarations go here
#include "statistics.hh"

#include "fixtures.h" // Header with the fixture declaration for each unit tests. It is not provided.
                      // It is up to students to configure the running environment of each test.
                      // Specifically, it determines how the CPU, caches and memory are arranged and related.
#include "gmock/gmock.h"
using namespace testing;


// While students have complete control over the fixtures and additional declarations, 
// these unit tests may not be modified, and whatever code that is added has to abide 
// to the rules and semantics that the provided code demands.

// Notice that every unit test is based on a fixture. This is where the `cpu` instance should be 
// initialized, along with everything else that the test may require (namely, cache configuration).
// Unit tests are supposed to be self-documenting, so the lack on internal documentation is intentional. 







// General cache behavior unit tests ---------------------------------------------------------
// These test target general cache behavior and are type agnostic.

TEST_F( CacheMechanics, instruction_type_entries_increase_instruction_reference_count )
{
    MemoryTrace trace( std::vector<std::string>{ "I  d,2", " L 2,1", "I  2,3" } );

    Statistics stats = cpu.run( trace );

    ASSERT_THAT( stats[ Statistics::instruction_references ], Eq(2u) );
}

TEST_F( CacheMechanics, store_and_load_type_entries_increase_data_reference_count )
{
    MemoryTrace trace( std::vector<std::string>{ "I  d,2", " L 2,1", " S 4,2" } );

    Statistics stats = cpu.run( trace );

    ASSERT_THAT( stats[ Statistics::data_references ], Eq(2u) );
}

TEST_F( CacheMechanics, first_access_is_cold_miss )
{
    MemoryTrace trace( std::vector<std::string>{ "I  d,2", "I  d,1" } );

    Statistics stats = cpu.run( trace );

    ASSERT_THAT( stats[Statistics::cache_misses], Eq(1u) );
    ASSERT_THAT( stats[Statistics::instruction_references],Eq(2u) );
}

TEST_F( CacheMechanics, hits_when_addresses_are_cached )
{
    MemoryTrace trace( {
                "I  0,1", // never referenced -> cold miss
                "I  0,1", // already referenced -> hit
                "I  1,1", // same block as previous -> hit
                "I  2,1", // never referenced -> cold miss
                "I  3,1", // same block as previous -> hit 
                "I  2,1"  // already referenced -> hit
            } );

    Statistics stats = cpu.run( trace );

    ASSERT_THAT( stats[Statistics::cache_misses], Eq(2u) );
    ASSERT_THAT( stats[Statistics::cold_misses], Eq(2u) );
    ASSERT_THAT( stats[Statistics::cache_hits],Eq(4u) );
}






// Direct-mapped cache tests --------------------------------------------------------
// These unit tests are supposed to unit test a direct mapped cache type
// It should be defined in terms of the S, B, E, m tuple and meet the documented requirements:
// 4 sets, 1 line per set, 2 bytes per block

TEST_F( DirectMappedCacheTest, properties_match_the_documented_requirements )
{
    auto & direct_mapped_cache = cpu.get_cache(0);

    ASSERT_THAT( direct_mapped_cache.associativity, Eq(1u) );
    ASSERT_THAT( direct_mapped_cache.set_count,     Eq(4u) );
    ASSERT_THAT( direct_mapped_cache.block_size,    Eq(2u) );
    ASSERT_THAT( direct_mapped_cache.size,          Eq(8u) );
}

TEST_F( DirectMappedCacheTest, references_are_bound_to_the_set_index_part_of_the_address )
{
    MemoryTrace trace( {
                "I  0,1", // set 0, cold miss
                "I  2,1", // set 1, cold miss
                "I  5,1", // set 2, cold miss
                "I  c,1", // set 2, conflict miss
                "I  a,1", // set 1, conflict miss
                "I  7,1", // set 3, cold miss
                "I  6,1", // set 3, hit
                "I  1,1"  // set 0, hit
            } );

    auto stats = cpu.run( trace );

    ASSERT_THAT( stats[ Statistics::cold_misses ], Eq(4u) );
    ASSERT_THAT( stats[ Statistics::conflict_misses ], Eq(2u) );
    ASSERT_THAT( stats[ Statistics::cache_hits ], Eq(2u) );
}

// this is a unit test for the simulation of the exercise covered on the whiteboard,
// with the exact same configuration and memory accesses
// review texbook page 601 to 603 for a detailed walkthrough
TEST_F( DirectMappedCacheTest, class_example_behaves_as_described_on_the_whiteboard )
{
    MemoryTrace trace( {
                "I  0,1", // cold miss
                "I  1,1", // hit
                "I  d,1", // cold miss
                "I  8,1", // conflict miss
                "I  0,1", // conflict miss
            } );

    auto stats = cpu.run( trace );    

    ASSERT_THAT( stats[ Statistics::cold_misses ], Eq(2u) );
    ASSERT_THAT( stats[ Statistics::conflict_misses ], Eq(2u) );
    ASSERT_THAT( stats[ Statistics::cache_hits ], Eq(1u) );
}


// Set-associative cache tests --------------------------------------------------------
// These unit tests are supposed to unit test a set associative cache type
// It should be defined in terms of the S, B, E, m tuple and meet the documented requirements:
// 2 sets, 2 lines per set, 2 bytes per block

TEST_F( SetAssociativeCacheTest, properties_match_the_documented_requirements )
{
    auto & set_associative_cache = cpu.get_cache(0);

    ASSERT_THAT( set_associative_cache.associativity, Eq(2u) );
    ASSERT_THAT( set_associative_cache.set_count,     Eq(2u) );
    ASSERT_THAT( set_associative_cache.block_size,    Eq(2u) );
    ASSERT_THAT( set_associative_cache.size,          Eq(8u) );
}


TEST_F( SetAssociativeCacheTest, set_overlapping_lines_can_be_stored_in_different_lines )
{
    MemoryTrace trace( {
            "I  0,1", 
            "I  8,1",
            "I  0,1",
            "I  8,1"
    } );
            
    auto stats = cpu.run( trace );

    ASSERT_THAT( stats[Statistics::cold_misses], Eq(2u) );
    ASSERT_THAT( stats[Statistics::conflict_misses], Eq(0u) );
    ASSERT_THAT( stats[Statistics::cache_hits], Eq(2u) );
}

// test that proves the different behavior for a set-associative cache
// when running the same class example that was used for the direct mapped cache
// this is a slightly modified of the class example
TEST_F( SetAssociativeCacheTest, class_example_produces_less_conflicts )
{
    MemoryTrace trace( {
                "I  0,1", // cold miss -> set 0, take 1 free line
                "I  1,1", // hit
                "I  d,1", // cold miss -> set 0, take 1 free line
                "I  a,1", // cold miss -> set 1, take 1 free line
                "I  0,1", // hit -> third reference didn't evict this one
            } );

    auto stats = cpu.run( trace );    

    ASSERT_THAT( stats[ Statistics::cold_misses ], Eq(3u) );
    ASSERT_THAT( stats[ Statistics::conflict_misses ], Eq(0u) );
    ASSERT_THAT( stats[ Statistics::cache_hits ], Eq(2u) );
}







// Fully associative cache tests --------------------------------------------------------
// These unit tests are supposed to unit test a fully associative cache type
// It should be defined in terms of the S, B, E, m tuple and meet the documented requirements:
// 1 sets, 4 lines per set, 2 bytes per block

TEST_F( FullyAssociativeCacheTest, properties_match_the_documented_requirements )
{
    auto & fully_associative_cache = cpu.get_cache(0);

    ASSERT_THAT( fully_associative_cache.associativity, Eq(4u) );
    ASSERT_THAT( fully_associative_cache.set_count,     Eq(1u) );
    ASSERT_THAT( fully_associative_cache.block_size,    Eq(2u) );
    ASSERT_THAT( fully_associative_cache.size,          Eq(8u) );
}

TEST_F( FullyAssociativeCacheTest, all_lines_are_used_until_capacity_is_filled )
{
    MemoryTrace trace( {
                "I  0,1",
                "I  4,1",
                "I  8,1",
                "I  A,1",
            } );

    auto stats = cpu.run( trace );
    auto & l1 = cpu.get_cache(0);

    ASSERT_THAT( stats[Statistics::conflict_misses], Eq(0u) );
    ASSERT_THAT( l1.is_address_cached(0), Eq(true) );
    ASSERT_THAT( l1.is_address_cached(4), Eq(true) );
    ASSERT_THAT( l1.is_address_cached(8), Eq(true) );
    ASSERT_THAT( l1.is_address_cached(10), Eq(true) );
}

// test that proves the different behavior for a fully associative cache
// when running the same class example that was used for the direct mapped cache
// there is no set index in the address decomposition, so all address bits but the offset are used for the tag field
TEST_F( FullyAssociativeCacheTest, class_example_produces_the_same_results_as_2_way_associative_cache )
{
    MemoryTrace trace( {
                "I  0,1", // cold miss -> tag 000
                "I  1,1", // hit -> tag 000, offset 1
                "I  d,1", // cold miss -> tag 110
                "I  8,1", // cold miss -> tag 100
                "I  0,1", // hit -> tag 000
            } );

    auto stats = cpu.run( trace );    

    ASSERT_THAT( stats[ Statistics::cold_misses ], Eq(3u) );
    ASSERT_THAT( stats[ Statistics::conflict_misses ], Eq(0u) );
    ASSERT_THAT( stats[ Statistics::cache_hits ], Eq(2u) );
}







// Line eviction policy tests -----------------------------------------------------------
// These tests target the different line eviction policies that a cache may implement
// Line eviction occurs when there is a conflict miss and a requested block has to be
// stored in a line that is holding previously cached data. In this case, a line is 
// selected by the eviction policy and its contents are replaced by the new block.
// Line eviction policies are to be tested against a fully associative cache type.

// this is not a useful test; its only purpose is to force a fully associative cache type on the fixture
// this is not enforced for the different eviction policy tests, but the expectations are set against this
// type of cache. Specific eviction policy fixtures could be derived from this one.
TEST_F( LineEvictionPolicyTest, properties_match_the_documented_requirements )
{
    auto & fully_associative_cache = cpu.get_cache(0);

    ASSERT_THAT( fully_associative_cache.associativity, Eq(4u) );
    ASSERT_THAT( fully_associative_cache.set_count,     Eq(1u) );
    ASSERT_THAT( fully_associative_cache.block_size,    Eq(2u) );
    ASSERT_THAT( fully_associative_cache.size,          Eq(8u) );
}

// Random policy: upon conflict, select a random line to evict
TEST_F( RandomLineEvictionPolicyTest, random_policy_evicts_a_random_line )
{
    MemoryTrace trace( {
                "I  0,1",
                "I  2,1",
                "I  4,1",
                "I  6,1", // at this point the cache should be full
                "I  8,1", // this conflict misses and replaces a random line
                "I  8,1"  // this should hit, although is impossible to tell on which line will it be mapped
            } );

    auto stats = cpu.run( trace );

    ASSERT_THAT( stats[ Statistics::cold_misses ], Eq(4u) );
    ASSERT_THAT( stats[ Statistics::conflict_misses ], Eq(1u) );
    ASSERT_THAT( stats[ Statistics::cache_hits ], Eq(1u) );
}

// LRU policy: upon conflict, select the least recently used line to evict
TEST_F( LRULineEvictionPolicyTest, LRU_policy_evicts_least_recently_used_line )
{
    MemoryTrace trace( {
                "I  0,1", // this is the oldest memory reference (LRU)
                "I  2,1",
                "I  4,1",
                "I  6,1",
                // at this point the cache should be full
                "I  8,1"  // this conflict misses and replaces the least recently used line (containing addresses 0 and 1)
            } );

    auto stats = cpu.run( trace );
    auto & l1 = cpu.get_cache( 0 );

    ASSERT_THAT( stats[ Statistics::cold_misses ], Eq(4u) );
    ASSERT_THAT( stats[ Statistics::conflict_misses ], Eq(1u) );

    ASSERT_THAT( l1.is_address_cached(0), Eq(false) ); // address 0 was evicted
}

// LFU policy: upon conflict, select the least frequently used line to evict
TEST_F( LFULineEvictionPolicyTest, LFU_policy_evicts_least_frequently_used_line )
{
    MemoryTrace trace( {
                "I  0,1",
                "I  0,1",
                "I  2,1", // this address is only referenced once (LFU)
                "I  4,1",
                "I  4,1",
                "I  6,1",
                "I  6,1",
                "I  8,1"  // this conflict misses and replaces the line that contains addresses 2 and 3
            } );

    auto stats = cpu.run( trace );
    auto & l1 = cpu.get_cache( 0 );

    ASSERT_THAT( stats[ Statistics::cold_misses ], Eq(4u) );
    ASSERT_THAT( stats[ Statistics::conflict_misses ], Eq(1u) );
    ASSERT_THAT( stats[ Statistics::cache_hits ], Eq(3u) );

    ASSERT_THAT( l1.is_address_cached(2), Eq(false) ); // block containing address 2 was evicted
}




//---------------------------------------------------------------------------
/**
* @file		cache.h
* @date 	4/20/2019
* @author	Alexandre Corcia Aguilera
* @par		Login: alexandre.corcia
* @par		Course: CS315
* @par		Assignment #5
* @brief 	The cache mechanics are here, here we have the Constructor
*			with all of the variable setting and allocating the memory
*			and some function on the work of the cache.
**/
//---------------------------------------------------------------------------
#pragma once
#include "trace.hh"
#include "statistics.hh"
#include <queue>
#include "memory.hh"

class CACHE
{
public: //PUBLIC VARIABLES
	enum EvictionPolicy {Line,Random,LRU,LFU};

	unsigned associativity;
	unsigned set_count;
	unsigned block_size;
	unsigned size;
	EvictionPolicy m_policy;

	struct set
	{
		struct line
		{
			bool valid{ false };
			unsigned tag{ 0 };

			std::vector<char> block;
		};

		std::vector<line *> s_all_lines;
		std::vector<unsigned int> s_al_frequencies;
	};
	std::vector<set *> c_all_sets;
	std::queue<unsigned int> past_lines;
	Memory<4> * m_memory;

private:

public: //PUBLIC FUNCTIONS
	CACHE(unsigned associ, unsigned count, unsigned b_size, unsigned size, EvictionPolicy policy);

	void set_adress(Statistics & stats, size_t new_address);
	bool is_address_cached(size_t);

};


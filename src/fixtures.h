//---------------------------------------------------------------------------
/**
* @file		fixtures.h
* @date 	4/20/2019
* @author	Alexandre Corcia Aguilera
* @par		Login: alexandre.corcia
* @par		Course: CS315
* @par		Assignment #5
* @brief 	This file has all of the classes for the tests that are in the Unit_test.cpp file
**/
//---------------------------------------------------------------------------
#pragma once
#include "gtest.h"
#include "cpu.h"


class CacheMechanics : public ::testing::Test
{
public:
	CPU cpu;
	

	CacheMechanics()
	{ cpu = CPU(2, 2, 2, 2); }
};

class DirectMappedCacheTest : public ::testing::Test
{
public:
	CPU cpu;

	DirectMappedCacheTest()
	{
		cpu = CPU(1, 4, 2, 8);
	}
};

class SetAssociativeCacheTest : public ::testing::Test
{
public:
	CPU cpu;

	SetAssociativeCacheTest()
	{
		cpu = CPU(2, 2, 2, 8);
	}
};


class FullyAssociativeCacheTest : public ::testing::Test
{
public:
	CPU cpu;

	FullyAssociativeCacheTest()
	{
		cpu = CPU(4, 1, 2, 8);
	}
};


class LineEvictionPolicyTest : public ::testing::Test
{
public:
	CPU cpu;

	LineEvictionPolicyTest()
	{
		cpu = CPU(4, 1, 2, 8);
	}

};


class RandomLineEvictionPolicyTest : public ::testing::Test
{
public:
	CPU cpu;

	RandomLineEvictionPolicyTest()
	{
		CACHE::EvictionPolicy ep = CACHE::EvictionPolicy::Random;
		cpu = CPU(4, 1, 2, 8, ep);
	}
};

class LRULineEvictionPolicyTest : public ::testing::Test
{
public:
	CPU cpu;

	LRULineEvictionPolicyTest()
	{
		CACHE::EvictionPolicy ep = CACHE::EvictionPolicy::LRU;
		cpu = CPU(4, 1, 2, 8, ep);
	}
};

class LFULineEvictionPolicyTest : public ::testing::Test
{
public:
	CPU cpu;

	LFULineEvictionPolicyTest()
	{
		CACHE::EvictionPolicy ep = CACHE::EvictionPolicy::LFU;
		cpu = CPU(4, 1, 2, 8, ep);
	}

};

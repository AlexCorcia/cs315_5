
#include "gmock/gmock.h"

// use this main function as an entry point if you want to run gmock tests
// replace this with your main function code otherwise
int main( int argc, char ** argv )
{
    // init gmock
    testing::InitGoogleMock( &argc, argv );
    // run registered tests
	auto result = RUN_ALL_TESTS();

	return result;

}

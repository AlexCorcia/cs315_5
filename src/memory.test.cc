#include "memory.hh"

#include "gmock/gmock.h"
using namespace testing;

// Memory unit tests ------------------------------------------------------------------

TEST( MemoryTesting, address_space_parameter_defines_the_power_of_2_addressable_bytes )
{
    Memory<8> mem_256b;

    ASSERT_THAT( sizeof(mem_256b), Eq( 256u ) );
}

TEST( MemoryTesting, subscript_provides_input_output_access_to_a_byte_in_memory )
{
    Memory<8> mem_256b;

    mem_256b[22] = 0xDD;

    ASSERT_THAT( mem_256b[22], Eq( 0xDD ) );
}

//---------------------------------------------------------------------------
/**
* @file		cpu.h
* @date 	4/20/2019
* @author	Alexandre Corcia Aguilera
* @par		Login: alexandre.corcia
* @par		Course: CS315
* @par		Assignment #5
* @brief 	This is the simulation of the Cpu that will be taking to the cache.
**/
//---------------------------------------------------------------------------

#include "cpu.h"


/**
* @brief The constructor of the class Cpu that will pass all of the information to the Cache.
*Same variables as the default constructor of the cache.
*/
CPU::CPU(unsigned a, unsigned sc, unsigned bs, unsigned s, CACHE::EvictionPolicy ep)
{
	m_cache = new CACHE(a, sc, bs, s, ep);
}


/**
* @brief Depending on the Entry of the memory Trace we will be be calling and setting the stats diferently.
* @param MemoryTrace	The values were we will get the instructions, the Storing or the Loading.
*/
Statistics CPU::run(MemoryTrace m_trace)
{
	Statistics result = Statistics();

	//Going through all of the MemoryTrace.
	for (auto it = m_trace.begin(); it != m_trace.end(); it++)
	{
		//Chekcing if it is an instruction, and if it is call the addres set
		if (it->type == MemoryTrace::Entry::event_type::instruction)
		{
			result[Statistics::instruction_references]++;
			m_cache->set_adress(result, it->address);
		}

		if (it->type == MemoryTrace::Entry::event_type::memory_store)
		{
			result[Statistics::data_references]++;
			result[Statistics::store_instructions]++;
		}

		if (it->type == MemoryTrace::Entry::event_type::memory_load)
		{
			result[Statistics::data_references]++;
			result[Statistics::load_instructions]++;
		}
	}
	return result;
}

CACHE CPU::get_cache(unsigned int m_value)
{
	return *m_cache;
}
